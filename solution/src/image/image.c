#include "image.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* Creates empty image of given sizes */
struct image image_create(const size_t width, const size_t height) {
    struct pixel* data = malloc(width * height * sizeof(struct pixel));
    return (struct image) {.width = width, .height = height, .data = data};
}

/* Deallocates image's data */
void image_delete(const struct image image) {
    free(image.data);
}

/* Returns pixel in given position */
struct pixel image_get_pixel(const struct image image, const size_t row, const size_t column) {
    return image.data[row * image.width + column];
}

/* Sets pixel in given position */
bool image_set_pixel(const struct image image, const size_t row, const size_t column, const struct pixel pixel) {
    bool is_successful = false;
    if ((row < image.height) && (column < image.width)) {
        image.data[row * image.width + column] = pixel;
        is_successful = true;
    }
    return is_successful;
}

/* Returns the size of image */
size_t image_calculate_size(const struct image image) {
    return sizeof(struct pixel) * image.width * image.height;
}

