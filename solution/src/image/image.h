#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image image_create(const size_t width, const size_t height);

void image_delete(const struct image image);

struct pixel image_get_pixel(const struct image image, const size_t row, const size_t column);

bool image_set_pixel(const struct image image, const size_t row, const size_t column, const struct pixel pixel);

size_t image_calculate_size(const struct image image);

#endif //LAB3_IMAGE_H
