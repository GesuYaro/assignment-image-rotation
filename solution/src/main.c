#include <stdio.h>

#include "file/file.h"
#include "image/image.h"
#include "transforms/transforms.h"

int main( int argc, char** argv ) {
    //(void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc < 3) {
        fprintf(stderr, "Usage: image-transformer <source-image> <transformed-image>\n");
        return 0;
    }

    const char* const input_file_name = argv[1];
    const char* const output_file_name = argv[2];
    struct image input_image = {0};
    const enum read_status image_read_status = image_read_file_bmp(input_file_name, &input_image);
    fprintf(stderr, "image read status: %d\n", image_read_status);
    if (image_read_status != READ_OK) {
        return 0;
    }

    struct image transformed_image = {0};
    bool transform_status = rotate(input_image, &transformed_image);

    fprintf(stderr, "image transform status: %d\n", transform_status);

    const enum write_status image_write_status = image_write_file_bmp(output_file_name, transformed_image);

    fprintf(stderr, "image write status: %d\n", image_write_status);

    image_delete(input_image);
    image_delete(transformed_image);

    return 0;
}
