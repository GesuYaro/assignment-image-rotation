#ifndef LAB3_TRANSFORMS_H
#define LAB3_TRANSFORMS_H

#include "../image/image.h"

bool rotate(const struct image source, struct image* const target);

#endif //LAB3_TRANSFORMS_H
