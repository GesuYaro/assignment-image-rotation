#include "transforms.h"

#include <stdbool.h>
#include <stdint.h>

#include "../image/image.h"

bool rotate(const struct image source, struct image* target) {
    const size_t target_width = source.height;
    const size_t target_height = source.width;
    *target = image_create(target_width, target_height);
    if (!target->data) return false;
    for (size_t i = 0; i < target_height; i++) {
        for (size_t j = 0; j < target_width; j++) {
            struct pixel buffer = image_get_pixel(source, j, i);
            image_set_pixel(*target, i, target->width - j - 1, buffer);
        }
    }
    return true;
}
