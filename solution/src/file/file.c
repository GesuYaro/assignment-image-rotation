#include "file.h"

#include <errno.h>
#include <stdio.h>

#include "../bmp_format/bmp_format.h"
#include "../image/image.h"
#include "../util/io_codes.h"

enum read_status image_read_file_bmp(const char* const filename, struct image* const image) {
    FILE * const fp = fopen(filename, "rb");
    if (errno != 0) return READ_CANT_OPEN_FILE;
    const enum read_status image_read_result = image_read_bmp(image, fp);
    if (image_read_result != READ_OK) return image_read_result;
    fclose(fp);
    if (errno != 0) return READ_CANT_CLOSE_FILE;
    return READ_OK;
}

enum write_status image_write_file_bmp(const char* const filename, const struct image image) {
    FILE * const fp = fopen(filename, "wb");
    if (errno != 0) return WRITE_CANT_OPEN_FILE;
    const enum write_status image_write_result = image_write_bmp(image, fp);
    if (image_write_result != WRITE_OK) return image_write_result;
    fclose(fp);
    if (errno !=0) return WRITE_CANT_CLOSE_FILE;
    return WRITE_OK;
}

