#ifndef LAB3_FILE_H
#define LAB3_FILE_H

#include "../image/image.h"
#include "../util/io_codes.h"

enum read_status image_read_file_bmp(const char* const filename, struct image* const image);
enum write_status image_write_file_bmp(const char* const filename, const struct image image);

#endif //LAB3_FILE_H
