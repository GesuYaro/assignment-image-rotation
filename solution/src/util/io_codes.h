#ifndef LAB3_IO_CODES_H
#define LAB3_IO_CODES_H

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_CANT_OPEN_FILE,
    READ_CANT_CLOSE_FILE,
    READ_CANT_CREATE_IMAGE
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_PADDING_ERROR,
    WRITE_CANT_OPEN_FILE,
    WRITE_CANT_CLOSE_FILE
};

#endif //LAB3_IO_CODES_H
