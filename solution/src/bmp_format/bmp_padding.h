#ifndef LAB3_BMP_PADDING_H
#define LAB3_BMP_PADDING_H

#include <stdint.h>

uint8_t get_padding(const uint32_t width);

#endif //LAB3_BMP_PADDING_H
