#ifndef LAB3_BMP_FORMAT_H
#define LAB3_BMP_FORMAT_H

#include <stdio.h>

#include "../image/image.h"
#include "../util/io_codes.h"

enum read_status image_read_bmp(struct image* const image, FILE* const file);
enum write_status image_write_bmp(const struct image image, FILE* const file);

#endif //LAB3_BMP_FORMAT_H
