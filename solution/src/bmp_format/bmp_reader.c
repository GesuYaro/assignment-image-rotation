#include "bmp_format.h"

#include <stdint.h>
#include <stdlib.h>

#include "bmp_header.h"
#include "bmp_padding.h"

#include "../image/image.h"
#include "../util/io_codes.h"

static enum read_status read_header(FILE* const file, struct bmp_header* const header) {
    if (fread(header, sizeof(struct bmp_header), 1, file)) {
        return READ_OK;
    }
    return READ_INVALID_HEADER;
}

static enum read_status read_pixels(FILE* const file, const struct image * image) {
    const size_t height = image->height;
    const size_t width = image->width;
    struct pixel * const pixels = image->data;
    const uint8_t padding = get_padding(width);
    for (uint32_t i = 0; i < height; i++) {
        if (fread(pixels + (width * i), sizeof(struct pixel), width, file) != width) return READ_INVALID_SIGNATURE;
        if (fseek(file, padding, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status image_read_bmp(struct image* const image, FILE* const file) {
    struct bmp_header header = {0};
    const enum read_status header_status = read_header(file, &header);
    if (header_status != READ_OK) return header_status;
    *image = image_create(header.biWidth, header.biHeight);
    if (!image->data) return READ_CANT_CREATE_IMAGE;
    if (fseek(file, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_HEADER;
    return read_pixels(file, image);
}
