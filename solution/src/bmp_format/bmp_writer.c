#include "bmp_format.h"

#include <stdio.h>
#include <stdlib.h>

#include "bmp_header.h"
#include "bmp_padding.h"

#include "../image/image.h"

static const struct bmp_header HEADER_TEMPLATE =  {
        .bfType = 0x4d42,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biPlanes = 1,
        .biCompression = 0,
        .biXPelsPerMeter = 2835,
        .biYPelsPerMeter = 2835,
        .biBitCount = 24,
        .biClrUsed = 0,
        .biClrImportant = 0
};

static struct bmp_header create_header(const struct image image) {
    const size_t image_size = image_calculate_size(image);
    struct bmp_header header = HEADER_TEMPLATE;
    header.bfileSize = sizeof(struct bmp_header) + image_calculate_size(image);
    header.biWidth = image.width;
    header.biHeight = image.height;
    header.biSizeImage = image_size;
    return header;
}


static enum write_status write_header(const struct image image, FILE* const file) {
    const struct bmp_header header = create_header(image);
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) {
        return WRITE_HEADER_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_pixels(const struct image image, FILE* const file) {
    const uint8_t padding = get_padding(image.width);
    const size_t width = image.width;
    const size_t height = image.height;
    const uint8_t paddings[3] = {0};
    for (size_t i = 0; i < height; i++) {
        if (!fwrite((image.data) + i * width, sizeof(struct pixel) * width, 1, file)) {
            return WRITE_ERROR;
        }
        size_t written_padding = fwrite(paddings, padding, 1, file);
        if (padding) {
            if (!written_padding) return WRITE_PADDING_ERROR;
        } else {
            if (written_padding) return WRITE_PADDING_ERROR;
        }
    }
    return WRITE_OK;
}


enum write_status image_write_bmp(const struct image image, FILE* const file) {
    const enum write_status header_write_status = write_header(image, file);
    if (header_write_status != WRITE_OK) return header_write_status;
    return write_pixels(image, file);
}
